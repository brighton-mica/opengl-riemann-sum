#pragma once
#include <GL\glew.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <memory>

class Mesh {
public:
	Mesh();
	~Mesh();

	void createOrderedMesh(GLfloat* vertices, unsigned int numOfVertices, GLenum primitive);
	void renderOrderedMesh();
	void createMesh(std::vector<GLfloat> vertices, std::vector<unsigned int> indices, unsigned int numOfVertices, unsigned int numOfIndices, GLenum primitive);
	void createMesh(GLfloat* vertices, unsigned int* indices, unsigned int numOfVertices, unsigned int numOfIndices, GLenum primitive);
	void renderMesh();
	void clearMesh();

private:
	GLuint VAO, VBO, IBO;
	GLsizei indexCount;
	GLenum glPrimitive;
	static unsigned int meshId;
	unsigned int id;
};

