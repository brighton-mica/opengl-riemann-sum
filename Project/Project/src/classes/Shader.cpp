#include "Shader.h"

Shader::Shader() {
	shaderID = 0;
	uniformModel = 0;
	uniformProjection = 0;
	uniformColor = 0;
}

Shader::~Shader() {
	std::cout << "Shader " << shaderID << " destructed" << std::endl;
	clearShader();
}

void Shader::createFromString(const char* vertexCode, const char* fragmentCode) {
	compileShader(vertexCode, fragmentCode);
}

void Shader::createFromFiles(const char* vertexLocation, const char* fragmentLocation) {
	std::string vertexString = readFile(vertexLocation);
	std::string fragmentString = readFile(fragmentLocation);
	const char* vertexCode = vertexString.c_str();
	const char* fragmentCode = fragmentString.c_str();

	compileShader(vertexCode, fragmentCode);
}


#include <direct.h>
#define GetCurrentDir _getcwd

std::string Shader::readFile(const char* fileLocation) {
	std::string content;
	std::ifstream fileStream(fileLocation, std::ios::in);

	if (!fileStream.is_open()) {
		std::cout << "Failed to read " << fileLocation << " file!" << std::endl;
		return "";
	}

	std::string line = "";
	while (!fileStream.eof()) {
		std::getline(fileStream, line);
		content.append(line + "\n");
	}

	fileStream.close();
	return content;
}

GLuint Shader::getProjectionLocation() {
	return uniformProjection;
}

GLuint Shader::getModelLocation() {
	return uniformModel;
}

GLuint Shader::getColorLocation() {
	return uniformColor;
}

void Shader::compileShader(const char* vertexCode, const char* fragmentCode) {
	// Creates a program ont he grpahics card and assigns shader the ID 
	shaderID = glCreateProgram();

	if (!shaderID) {
		std::cout << "ERROR creating shader program!" << std::endl;
		return;
	}

	addShader(shaderID, vertexCode, GL_VERTEX_SHADER);
	addShader(shaderID, fragmentCode, GL_FRAGMENT_SHADER);

	GLint result = 0;
	GLchar eLog[1024] = { 0 };

	// (Link) Create executables on the graphics card
	glLinkProgram(shaderID);
	glGetProgramiv(shaderID, GL_LINK_STATUS, &result);
	if (!result) {
		glGetProgramInfoLog(shaderID, sizeof(eLog), NULL, eLog);
		std::cout << "Error linking program " << eLog << std::endl;
		return;
	}

	// Validate program
	glValidateProgram(shaderID);
	glGetProgramiv(shaderID, GL_VALIDATE_STATUS, &result);
	if (!result) {
		glGetProgramInfoLog(shaderID, sizeof(eLog), NULL, eLog);
		std::cout << "Error validating program " << eLog << std::endl;
		return;
	}

	uniformProjection = glGetUniformLocation(shaderID, "projection");
	uniformModel = glGetUniformLocation(shaderID, "model");
	uniformColor = glGetUniformLocation(shaderID, "color");
	std::cout << "Shader " << shaderID << " created" << std::endl;
}

void Shader::addShader(GLuint program, const char* shaderCode, GLenum shaderType) {
	// Creates shader of "shaderType" on graphics card and sotres shader ID in "shader"
	GLuint shader = glCreateShader(shaderType);

	// Store shader code as a GLchar
	const GLchar* code[1];
	code[0] = shaderCode;

	// Store shader length as a single element GLint array
	GLint codeLength[1];
	codeLength[0] = strlen(shaderCode);

	// Add aource code to shader object
	// (shader, count, **string, *length)
	// shader - the ID of the shader object
	// 1 - the number of elements in our **string and *length arrays
	// **string - shader code
	// *length - length of shader code
	glShaderSource(shader, 1, code, codeLength);

	GLint result = 0;
	GLchar eLog[1024] = { 0 };

	// Compile the shader
	glCompileShader(shader);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
	if (!result) {
		glGetShaderInfoLog(shader, sizeof(eLog), NULL, eLog);
		std::cout << "Error compiling the " << shaderType << " shader! " << eLog << std::endl;
		return;
	}

	// Attach shader to program
	glAttachShader(program, shader);
}

void Shader::useShader() {
	if (shaderID == 0) {
		std::cout << "(Shader) ERROR - cant use program that doesn't exist" << std::endl;
		return;
	}
	glUseProgram(shaderID);
}

void Shader::clearShader() {
	if (shaderID != 0) {
		glDeleteProgram(shaderID);
		shaderID = 0;
	}

	uniformModel = 0;
	uniformProjection = 0;
	uniformColor = 0;
}