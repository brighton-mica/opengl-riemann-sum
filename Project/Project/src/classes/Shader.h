#pragma once
#include <iostream>
#include <string>
#include <fstream>

#include <GL\glew.h>

class Shader {
public:
	Shader();
	~Shader();

	void createFromString(const char* vertexCode, const char* fragmentCode);
	void createFromFiles(const char* vertexLocation, const char* fragmentLocation);
	std::string readFile(const char* fileLocation);
	GLuint getProjectionLocation();
	GLuint getModelLocation();
	GLuint getColorLocation();
	void useShader();
	void clearShader();

private:
	GLuint shaderID, uniformProjection, uniformModel, uniformColor;
	void compileShader(const char* vertexCode, const char* fragmentCode);
	void addShader(GLuint program, const char* shaderCode, GLenum shaderType);
};
