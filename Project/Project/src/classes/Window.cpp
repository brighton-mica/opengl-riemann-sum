#include "Window.h"

Window::Window() {
	width = 800;
	height = 600;
	window = NULL;
}

Window::Window(GLint windowWidth, GLint windowHeight) {
	width = windowWidth;
	height = windowHeight;
	window = NULL;
}

Window::~Window() {
	// Destroy window
	glfwDestroyWindow(window);

	// The terminate command will end all glfw processes
	glfwTerminate();
}

int Window::init() {
	// Initialize GLFW
	if (!glfwInit()) {
		std::cout << "GLFW Initialization failed!" << std::endl;

		// The terminate command will end all glfw processes
		glfwTerminate();
		return 1;
	}

	// --- Setup GLFW window properties --- //

	// The context we create is goign to have an OpenGL version of 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	// Ensure that we are not using depreicated features of OpenGL
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	// Create window
	window = glfwCreateWindow(width, height, "Window", NULL, NULL);

	if (!window) {
		std::cout << "GLFW window craetion failed!" << std::endl;

		// The terminate command will end all glfw processes
		glfwTerminate();
		return 1;
	}
	else
	{
		std::cout << "GLFW initialized" << std::endl;
	}

	// Get buffer size information
	glfwGetFramebufferSize(window, &bufferWidth, &bufferHeight);

	// Let GLEW know that the window we created is the one to draw to
	glfwMakeContextCurrent(window);

	// --- Setup GLEW --- //

	glEnable(GL_DEPTH_TEST);
	// glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	// Allow modern extensions
	glewExperimental = GL_TRUE;

	if (glewInit() != GLEW_OK) {
		std::cout << "GLEW initialization failed!" << std::endl;

		// Destroy window
		glfwDestroyWindow(window);

		// The terminate command will end all glfw processes
		glfwTerminate();
		return 1;
	}
	else
	{
		std::cout << "GLEW initialized" << std::endl;
	}

	// Set viewport size (what part of the screen can we draw on)
	glViewport(0, 0, bufferWidth, bufferHeight);
}

bool Window::shouldClose() {
	if (!window) {
		std::cout << "Window::shouldClose() ERROR - window is not defiend" << std::endl;
		return false;
	}
	return glfwWindowShouldClose(window);
}

void Window::swapBuffers() {
	if (!window) {
		std::cout << "Window::swapBuffers() ERROR - window is not defiend" << std::endl;
	}
	else {
		glfwSwapBuffers(window);
	}
}