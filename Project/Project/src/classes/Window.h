#pragma once
#include <iostream>

#include <GL\glew.h>
#include <GLFW\glfw3.h>

class Window 
{
	public:
		Window();
		Window(GLint windowWidth, GLint windowHeight);
		~Window();

		int init();
		bool shouldClose();
		void swapBuffers();
		GLint getBufferWidth() { return bufferWidth; }
		GLint getBufferHeight() { return bufferHeight; }
		GLFWwindow* getWindow() { return window; }

	private:
		GLFWwindow* window;
		GLint width, height;
		GLint bufferWidth, bufferHeight;
};

