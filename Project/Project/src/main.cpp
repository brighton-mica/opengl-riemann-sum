#include <iostream>
#include <algorithm>
#include <vector>

#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include "IMGUI/imgui.h"
#include "IMGUI/imgui_impl_glfw.h"
#include "IMGUI/imgui_impl_opengl3.h"
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>

#include "classes/Window.h"
#include "classes/Mesh.h"
#include "classes/Shader.h"

// Globals
const float PI = 3.14159265f;
const float toRadians = PI / 180.0f;

// Locals
static const GLint WINDOW_WIDTH = 800;
static const GLint WINDOW_HEIGHT = 650;
static const unsigned int X_POINTS = 200;
static const unsigned int VERTEX_ARRAY_SIZE = X_POINTS * 3;
static const unsigned int INDEX_ARRAY_SIZE = (X_POINTS * 2) - 2;


struct GameObjects
{
	std::vector<std::unique_ptr<Mesh>> axis;
	std::vector<std::unique_ptr<Mesh>> functions;
	std::vector<std::unique_ptr<Mesh>> rectangles;
	std::vector<std::unique_ptr<Mesh>> rectangleOutlines;
};

float GetArea(float(*func)(float x), int n)
{
	float deltaX = 1.0f / n;
	float x = deltaX;
	float area = 0.0f;

	for (int i = 1; i <= n; i++)
	{
		area += func(x) * deltaX;
		x += deltaX;
	}
	return area;
}

void GenerateRectangleOutlines(float(*func)(float x), int n, std::vector<std::unique_ptr<Mesh>>& objects)
{
	objects.clear();
	float deltaX = 1.0f / n;
	float x = deltaX, prevX = 0.0f;

	std::vector<GLfloat> vertices;
	std::vector<unsigned int> indices = { 0, 1, 1, 2, 2, 3, 3, 0 };

	for (int i = 0; i < n; i++)
	{
		vertices.insert(vertices.end(), {
			prevX, 0.0f, 0.0f,
			x, 0.0f, 0.0f,
			x, func(x), 0.0f,
			prevX, func(x), 0.0f,
			});

		std::unique_ptr<Mesh> mesh = std::make_unique<Mesh>();
		mesh->createMesh(vertices, indices, vertices.size(), 8, GL_LINES);
		objects.push_back(std::move(mesh));

		vertices.clear();

		prevX = x;
		x += deltaX;
	}
}

void GenerateRectangles(float(*func)(float x), int n, std::vector<std::unique_ptr<Mesh>> &objects)
{
	objects.clear();
	float deltaX = 1.0f / n;
	float x = deltaX, prevX = 0.0f;

	std::vector<GLfloat> vertices;
	std::vector<unsigned int> indices = { 0, 1, 2, 1, 2, 3 };

	for (int i = 0; i < n; i++)
	{
		vertices.insert(vertices.end(), { 
			prevX, 0.0f, 0.0f ,
			x, 0.0f, 0.0f,
			prevX, func(x), 0.0f,
			x, func(x), 0.0f
		});

		std::unique_ptr<Mesh> mesh = std::make_unique<Mesh>();
		mesh->createMesh(vertices, indices, vertices.size(), 6, GL_TRIANGLES);
		objects.push_back(std::move(mesh));

		vertices.clear();

		prevX = x;
		x += deltaX;
	}
}

void GenerateFunctions(std::vector<float(*)(float x)>& functions, GameObjects& objects)
{
	for (auto func : functions)
	{
		GLfloat vertices[VERTEX_ARRAY_SIZE] = { 0 };
		unsigned indices[INDEX_ARRAY_SIZE] = { 0 };
		float incr = 1.0f / (X_POINTS - 1), x = 0.0f;

		// Domain (x) : [0, 1]
		for (int i = 0; i < X_POINTS; i++)
		{
			vertices[3 * i] = x;
			vertices[3 * i + 1] = (GLfloat)func(x);
			vertices[3 * i + 2] = 0.0f;
			x += incr;
		}

		// Indices
		for (int i = 0; i < (X_POINTS - 1); i++)
		{
			indices[2 * i] = i;
			indices[2 * i + 1] = i + 1;
		}

		std::unique_ptr<Mesh> mesh = std::make_unique<Mesh>();
		mesh->createMesh(vertices, indices, VERTEX_ARRAY_SIZE, INDEX_ARRAY_SIZE, GL_LINES);
		objects.functions.push_back(std::move(mesh));
	}
}

void GenerateAxis(GameObjects &objects)
{
	GLfloat xVertices[VERTEX_ARRAY_SIZE] = { 0 };
	GLfloat yVertices[VERTEX_ARRAY_SIZE] = { 0 };
	unsigned indices[INDEX_ARRAY_SIZE] = { 0 };
	float incr = 1.0f / (X_POINTS - 1), x = 0;;

	// X AXIS - Domain [0, 1]
	for (int i = 0; i < X_POINTS; i++)
	{
		xVertices[3 * i] = x;
		xVertices[3 * i + 1] = 0.0f;
		xVertices[3 * i + 2] = 0.0f;
		x += incr;
	}

	// Y AXIS - Range [0, 1]
	x = 0.0f;
	for (int i = 0; i < X_POINTS; i++)
	{
		yVertices[3 * i] = 0.0f;
		yVertices[3 * i + 1] = x;
		yVertices[3 * i + 2] = 0.0f;
		x += incr;
	}

	// Indices
	for (int i = 0; i < (X_POINTS - 1); i++)
	{
		indices[2 * i] = i;
		indices[2 * i + 1] = i + 1;
	}

	std::unique_ptr<Mesh> mesh = std::make_unique<Mesh>();
	mesh->createMesh(xVertices, indices, VERTEX_ARRAY_SIZE, INDEX_ARRAY_SIZE, GL_LINES);
	objects.axis.push_back(std::move(mesh));

	mesh = std::make_unique<Mesh>();
	mesh->createMesh(yVertices, indices, VERTEX_ARRAY_SIZE, INDEX_ARRAY_SIZE, GL_LINES);
	objects.axis.push_back(std::move(mesh));
}

void CreateShaders(std::vector<std::unique_ptr<Shader>> &shaders)
{
	std::unique_ptr<Shader> shader = std::make_unique<Shader>();
	shader->createFromFiles("src/shaders/shader.vert", "src/shaders/shader.frag");
	shaders.push_back(std::move(shader));
}

int main()
{
	// WINDOW INIT
	Window window = Window(WINDOW_WIDTH, WINDOW_HEIGHT);
	if (!window.init()) { return 1; }

	// PROGRAM VAR INIT
	std::vector<float(*)(float x)> functions = {
		[](float x) { return glm::sin(x * PI); },
		[](float x) { return glm::cos(x * 0.5f * PI); },
		[](float x) { return x;  }
	};

	static GameObjects objects;
	std::vector<std::unique_ptr<Shader>> shaders;

	GenerateAxis(objects);
	GenerateFunctions(functions, objects);
	CreateShaders(shaders);

	GLuint uniformProjection = 0, uniformModel = 0, uniformColor = 0;
	glm::mat4 projection = glm::mat4(1.0f);
	// glm::mat4 projection = glm::perspective(glm::radians(45.0f), (GLfloat)window.getBufferWidth() / window.getBufferHeight(), 0.1f, 100.0f);

	int n = 10, prevN= 10, currentFunctionIndex = 0;
	static const char* functionLabels[] = { "sin(x*pi)", "cos(x*.5pi)", "x" };
	static const char* currentFunctionSelection = functionLabels[0];
	static const char* previousFunctionSelection = NULL;
	float area = GetArea(functions[currentFunctionIndex], n);

	GenerateRectangles(functions[currentFunctionIndex], n, objects.rectangles);
	GenerateRectangleOutlines(functions[currentFunctionIndex], n, objects.rectangleOutlines);

	// IMGUI INIT
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	ImGui::StyleColorsDark();
	ImGui_ImplGlfw_InitForOpenGL(window.getWindow(), true);
	ImGui_ImplOpenGL3_Init((char*)glGetString(GL_NUM_SHADING_LANGUAGE_VERSIONS));
	ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

	// GAME LOOP
	while (!window.shouldClose())
	{
		glfwPollEvents();
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		glClearColor(0.95f, 0.95f, 0.95f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// RENDERING - MESH
		shaders[0]->useShader();
		{
			uniformProjection = shaders[0]->getProjectionLocation();
			uniformModel = shaders[0]->getModelLocation();
			uniformColor = shaders[0]->getColorLocation();
			glm::mat4 model = glm::mat4(1.0f);
			model = glm::translate(model, glm::vec3(-0.9f, -0.9f, 0.0f));
			model = glm::scale(model, glm::vec3(1.8f, 1.8f, 1.0f));
			glUniformMatrix4fv(uniformProjection, 1, GL_FALSE, glm::value_ptr(projection));
			glUniformMatrix4fv(uniformModel, 1, GL_FALSE, glm::value_ptr(model));
			glUniform3f(uniformColor, 0.0f, 0.0f, 0.0f);
			std::for_each(objects.axis.begin(), objects.axis.end(), [](const auto& obj) { obj->renderMesh(); });
			std::for_each(objects.rectangleOutlines.begin(), objects.rectangleOutlines.end(), [](const auto& obj) { obj->renderMesh(); });
			objects.functions[currentFunctionIndex]->renderMesh();
			glUniform3f(uniformColor, 0.8f, 1.0f, 0.898f);
			std::for_each(objects.rectangles.begin(), objects.rectangles.end(), [](const auto& obj) { obj->renderMesh(); });
		}
		glUseProgram(0);

		// IMGUI
		{
			ImGui::Begin("Right Riemann Sum Visualizer");
			ImGui::SetNextItemWidth(100);

			if (ImGui::BeginCombo("f(x)", currentFunctionSelection)) // the second parameter is the label previewed before opening the combo.
			{
				for (short i = 0; i < IM_ARRAYSIZE(functionLabels); i++)
				{
					bool is_selected = (currentFunctionSelection == functionLabels[i]); // you can store your selection however you want, outside or inside your objects
					if (ImGui::Selectable(functionLabels[i], is_selected))
						currentFunctionSelection = functionLabels[i];
					if (is_selected)
					{
						ImGui::SetItemDefaultFocus();   // you may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
					}
				}
				ImGui::EndCombo();
			}
			ImGui::SetNextItemWidth(30);
			ImGui::InputInt("n", &n, 0, 0);
			ImGui::Text("Area: %f", area);
			ImGui::End();
			ImGui::Render();
			ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
		}

		// IMGUI EVENT-HANDLING
		{
			if (currentFunctionSelection != previousFunctionSelection)
			{
				auto it = std::find(functionLabels, functionLabels + 2, currentFunctionSelection);
				previousFunctionSelection = currentFunctionSelection;
				currentFunctionIndex = it - functionLabels;
				area = GetArea(functions[currentFunctionIndex], n);
				GenerateRectangles(functions[currentFunctionIndex], n, objects.rectangles);
				GenerateRectangleOutlines(functions[currentFunctionIndex], n, objects.rectangleOutlines);
			}
			else if (n != prevN && n > 0)
			{
				prevN = n;
				area = GetArea(functions[currentFunctionIndex], n);
				GenerateRectangles(functions[currentFunctionIndex], n, objects.rectangles);
				GenerateRectangleOutlines(functions[currentFunctionIndex], n, objects.rectangleOutlines);
			}
		}
		window.swapBuffers();
	}
}