#version 330

out vec4 RGB;
uniform vec3 color;

void main()
{
	RGB = vec4(color, 1.0);
}